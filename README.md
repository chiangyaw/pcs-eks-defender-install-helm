# Sample Script to Upgrade Prisma Cloud Defender AWS EKS with Gitlab CI via Helm

This is a sample CI script to install Prisma Cloud Defender on AWS EKS with Gitlab CI with helm chart 

Here is a quick summary on the workflow:
1. Get kubeconfig file for AWS EKS, so that the CI agent is able to connect to your kubenetes cluster.
2. Download twistcli from your dedicated Prisma Cloud tenants
3. Generate helm chart for Prisma Cloud Defender Deployment.
4. Create a namespace ```twistlock```.
5. Deploy new Defender with the kubernetes manifest file generated.

As part of this CI, you will need you setup the following as variables in your CI/CD configuration:
1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY
3. AWS_SESSION_TOKEN
4. PRISMACLOUD_USER
5. PRISMACLOUD_PASSWORD